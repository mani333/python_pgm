import java.io.*;
import java.util.Scanner;
class Main
{
 public static void main(String args[])
 {
	 Scanner in = new Scanner(System.in);
	 
	  String event1,event2;
	  String separator = ",";
	  System.out.println("Please enter employee ids of 1st event "); 
	  event1 = in.nextLine(); 
	  
	  System.out.println("Please enter employee ids of 2nd event ");
	  event2 = in.nextLine();
	  
	  String[] ev_array1 = event1.split(separator);   
	  String[] ev_array2 = event2.split(separator); 
	 
	get_unique_events(ev_array1, ev_array2);//Actual event calculation.
 }
 	
 public static void get_unique_events(String event1[],String event2[])//this method will find common emp ids between two events provided.
 {
  int k=0;
 // int coomom_ids[] = new int[4];
  for(int i=0;i<event1.length; i++)
  {
	  int ev1 = Integer.parseInt(event1[i]);
	  for(int j=0; j<event2.length; j++)
	  {
		  int ev2 = Integer.parseInt(event2[j]);
		  if(ev1==ev2)
		  {
			k++;
		  }
	  }
  } 
  System.out.println("Number of Common Employee ids are: "+k);
 }
}
