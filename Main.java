//The solutions should be written, for compiler compatibility : j2se jdk 6.0

import java.io.*; 
import java.util.*;
public class Main  
{  
 public static void main (String[] args) throws java.lang.Exception 
    { 
    
   //use the following code to fetch input from console 
     int input_hops[] = new int[1];//Int array to store input hops.
   	 
       
     Scanner in = new Scanner(System.in); 
     System.out.println("Enter Four Hops of Your Choice...");
     for(int i=0; i<1; i++)
     {
       input_hops[i]=in.nextInt();
     }
   	 calc_hop(input_hops);//Actual hop distance calculation will be held in this method.

    }
  public static void calc_hop(int input_hops[])//Method for hop distance calculation.
  {
	 int loop_hops_dist[] = {20,10,5};//Predefined in problem definition
   	 int distance = 0;
   	 int hop_count = 0;
	  for(int i=0; i<1; i++)
     {
       for(int j=0; j<input_hops[i]; j++)
       {
         distance = distance + loop_hops_dist[hop_count];
         hop_count++;
         if(hop_count > 2)
           hop_count = 0;
         
       }
       System.out.println("for hope "+input_hops[i]+" distance is "+distance);
       distance = 0;
       hop_count = 0;
     }
  }
}